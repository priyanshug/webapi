﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApi.repositories
{
    public class DoorRepository
    {
        public static SqlDataReader showDoors()
        {
            string CS = System.Configuration.ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            SqlCommand cmd = new SqlCommand("select * from door", con);
            con.Open();
            SqlDataReader rdr = cmd.ExecuteReader();

            return rdr;
        }
    }
}