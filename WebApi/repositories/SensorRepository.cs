﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApi.repositories
{
    public class SensorRepository
    {
        public static DataSet showSensors()
        {
            var ds = new DataSet();
            string CS = System.Configuration.ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            con.Open();
            var cmd = new SqlCommand("select * from sensor", con);
            var adapter = new SqlDataAdapter(cmd);
            

            adapter.Fill(ds);
            

            return ds;
        }

        /*public SqlDataReader showSensor(int id)
        {
            string CS = System.Configuration.ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            SqlCommand cmd = new SqlCommand("select * from sensor where id = @id", con);
            con.Open();
            SqlDataReader rdr = cmd.ExecuteReader();

            return rdr;
        }*/




    }
}