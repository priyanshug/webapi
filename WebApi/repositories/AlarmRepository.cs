﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApi.repositories
{
    public class AlarmRepository
    {
        public static SqlDataReader showAlarms()
        {
            string CS = System.Configuration.ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            SqlCommand cmd = new SqlCommand("select * from sensor", con);
            con.Open();
            SqlDataReader rdr = cmd.ExecuteReader();

            return rdr;
        }
    }
}