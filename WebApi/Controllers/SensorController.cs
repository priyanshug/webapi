﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApi.Models;
using WebApi.repositories;

namespace WebApi.Controllers
{
    [EnableCors(origins: "http://localhost:49380", headers: "*", methods: "*")]
    public class SensorController : ApiController
    {
        static string CS = "data source=.; database= Devices; integrated security=SSPI";
        SqlConnection con = new SqlConnection(CS);

        List<Sensor> sl = new List<Sensor>();
        public List<Sensor> Get()
        {
            
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Sensor", con);
            
            IDataReader rdr = cmd.ExecuteReader();
            while(rdr.Read())
            {
                Sensor s = new Sensor();
                s.Id = rdr.GetString(0);
                s.Name = rdr.GetString(1);
                s.Location = rdr.GetString(2);
                s.Status = rdr.GetString(3);

                sl.Add(s);
            }


            return sl;
            con.Close();
            /*
            return new HttpResponseMessage()
            {
                Content = rdr
            };*/
        }

        public Sensor GetSensor(string id)
        {
            int x = Int32.Parse(id);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Sensor where id = "+x, con);

            IDataReader rdr = cmd.ExecuteReader();
            Sensor s = new Sensor();
            try
            {
                while (rdr.Read())
                {
                    
                    s.Id = rdr.GetString(0);
                    s.Name = rdr.GetString(1);
                    s.Location = rdr.GetString(2);
                    s.Status = rdr.GetString(3);

                    
                }

            }
            catch (Exception)
            {

                throw;
            }
            return s;
            con.Close();
            /*
            return new HttpResponseMessage()
            {
                Content = rdr
            };*/
        }

        public HttpResponseMessage Post()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent("POST: Test message")
            };
        }

        [HttpPut]
        public HttpResponseMessage Put(string id, Sensor sensor)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Sensor SET Status = 'BUZZING' WHERE Id = "+id, con);

            int num = cmd.ExecuteNonQuery();
            
            
            return new HttpResponseMessage()
            {
                Content = new StringContent(""+num)
            };
        }
    }
}