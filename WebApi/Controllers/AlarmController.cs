﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Models;

namespace WebApi.Controllers
{
    [EnableCors(origins: "http://localhost:49380", headers: "*", methods: "*")]
    public class AlarmController : ApiController
    {
        List<Alarm> sl = new List<Alarm>();
        public List<Alarm> Get()
        {
            string CS = "data source=.; database= Devices; integrated security=SSPI";
            SqlConnection con = new SqlConnection(CS);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Alarm", con);

            IDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                Alarm s = new Alarm();
                s.Id = rdr.GetString(0);
                s.Name = rdr.GetString(1);
                s.Location = rdr.GetString(2);
                s.Status = rdr.GetString(3);

                sl.Add(s);
            }


            return sl;
            con.Close();
            /*
            return new HttpResponseMessage()
            {
                Content = rdr
            };*/
        }

        public HttpResponseMessage Post()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent("POST: Test message")
            };
        }

        public HttpResponseMessage Put()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent("PUT: Test message")
            };
        }
    }
}
