﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Model
{
    public class Sensor
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
    }
}