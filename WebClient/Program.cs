﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    public class Sensor
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
    }


    class Program
    {    
            /// <summary>
            /// Http client responsible to add, delete devices and buzz alarm by sending http request to the API
            /// </summary>
            static HttpClient client = new HttpClient();


            /// <summary>
            /// Request the API and fetch all sensor data
            /// </summary>
            /// <param name="Sensor"></param>
            static void ShowSensor(Sensor Sensor)
            {
                Console.WriteLine($"Name: {Sensor.Name}\tStatus: " +
                    $"{Sensor.Status}\tLocation: {Sensor.Location}");
            }

            /// <summary>
            /// Create the Smoke sensors by sending the http request for the same
            /// </summary>
            /// <param name="Sensor"></param>
            /// <returns></returns>
            static async Task<Uri> CreateSensorAsync(Sensor Sensor)
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(
                    "api/Sensor", Sensor);
                response.EnsureSuccessStatusCode();

                // return URI of the created resource.
                return response.Headers.Location;
            }

            /// <summary>
            /// fetch the specific smoke sensor information
            /// </summary>
            /// <param name="path"></param>
            /// <returns></returns>
            static async Task<Sensor> GetSensorAsync(string path)
            {
                Sensor Sensor = null;
                HttpResponseMessage response = await client.GetAsync(path);
                if (response.IsSuccessStatusCode)
                {
                    Sensor = await response.Content.ReadAsAsync<Sensor>();
                }
                return Sensor;
            }

            /// <summary>
            /// Sends the signal that sensor has detected fire
            /// </summary>
            /// <param name="Sensor"></param>
            /// <returns></returns>
            static async Task<Sensor> UpdateSensorAsync(Sensor Sensor)
            {
                
                HttpResponseMessage response = await client.PutAsJsonAsync($"api/Sensor/{Sensor.Id}", Sensor);
            // response.EnsureSuccessStatusCode();

            // Deserialize the updated Sensor from the response body.
            //Sensor sensor = await response.Content.ReadAsAsync<Sensor>();
            Sensor sensor = null;
            return sensor;
            }

            /// <summary>
            /// Deletes the sensor
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            static async Task<HttpStatusCode> DeleteSensorAsync(string id)
            {
                HttpResponseMessage response = await client.DeleteAsync(
                    $"api/Sensors/{id}");
                return response.StatusCode;
            }

            static void Main()
            {
                 RunAsync().GetAwaiter().GetResult();
            }

            static async Task RunAsync()
            {
                // Update port # in the following line.
                client.BaseAddress = new Uri("http://localhost:63779/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                Console.WriteLine("Press Enter after Api is online");

                while (true)
                {

                    Console.WriteLine("\nWant to Add an alarm? Press 1 \nWant to Buzz an alarm? Press 2 \nWant to Delete an alarm? Press 3");
                    //===========================================================================================
                    Console.ReadLine();
                    Console.WriteLine("\nEnter Choice : ");
                    char ch = (char)Console.Read();
                    //Console.WriteLine("ch : "+ch);
                    int caseSwitch = ch; caseSwitch -= 48;

                    //Console.WriteLine("caseSwitch : "+caseSwitch);
                    string alarmId;Uri url;Sensor sensor;
                    switch (caseSwitch)
                    {
                        
                        case 1:
                            /*Console.WriteLine("Case 1");

                            Console.WriteLine("Create Alarm :");
                            Console.ReadLine();
                            alarmName = Console.ReadLine();

                            // Create a new Sensor
                            Sensor Sensor = new Sensor
                            {
                                Id = alarmName,
                                Name = alarmName,
                                Status = "0",
                                Location = "PSN"
                            };

                            var url = await CreateSensorAsync(Sensor);
                            Console.WriteLine($"Created Alarm Successfully\n");
                            //Console.WriteLine($"Created at {url}");
                            */

                            break;
                        case 2:
                            Console.WriteLine("Case 2");
                            Console.WriteLine("Buzz Alarm :");
                            Console.ReadLine();
                            alarmId = Console.ReadLine();

                            url = new Uri("http://localhost:63779/api/Sensor/" + alarmId);


                        // Get the Sensor
                          sensor = await GetSensorAsync(url.PathAndQuery);
                          ShowSensor(sensor);
                        //await PutSensorAsync(alarmId);

                            // Update the Sensor
                            Console.WriteLine("Updating Status...");
                            //sensor.Status = "1";
                            await UpdateSensorAsync(sensor);
                            Console.WriteLine($"Updated Alarm Successfully\n");

                            break;

                        case 3:
                            /*Console.WriteLine("Case 3");
                            Console.WriteLine("Delete Alarm :");
                            Console.ReadLine();
                            alarmName = Console.ReadLine();
                            url = new Uri("http://localhost:63779/api/Sensor/" + alarmName);


                            // Get the updated Sensor
                            Sensor = await GetSensorAsync(url.PathAndQuery);
                            ShowSensor(Sensor);


                            Console.WriteLine("#" + alarmName + "#");
                            Sensor.Id = alarmName;

                            // Delete the Sensor
                            var statusCode = await DeleteSensorAsync(Sensor.Id);
                            Console.WriteLine($"Deleted Alarm Successfully\n");
                            //Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");*/
                            break;

                        default:
                            Console.WriteLine("Default case");
                            Console.ReadLine();
                            break;
                    }

                    Console.ReadLine();
                }


            }

        }
    }

